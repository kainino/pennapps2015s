#!/usr/bin/env python3

# Copied from: https://github.com/mathisonian/simple-testing-server
# Modified by: Kai Ninomiya

from http.server import HTTPServer
from http.server import BaseHTTPRequestHandler
import cgi
import json
#from session_record import *


#cam = Campaign()


class JSONRequestHandler (BaseHTTPRequestHandler):

    def wr(self, s):
        return self.wfile.write(bytes(s, encoding='utf-8'))

    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        output = open("index.html", 'r').read()
        self.wr(output)

    def do_POST(self):
        try:
            self.send_response(200)
            self.wr('Content-Type: application/json\n')
            self.wr('Client: %s\n' % str(self.client_address))
            self.wr('User-agent: %s\n' % str(self.headers['user-agent']))
            self.wr('Path: %s\n' % self.path)
            self.end_headers()

            environ = {
                'REQUEST_METHOD': 'POST',
                'CONTENT_TYPE': self.headers['Content-Type'],
                }
            form = cgi.FieldStorage(
                    fp = self.rfile,
                    headers = self.headers,
                    environ = environ)

            if "cmd" in form:
                cmd = form["cmd"]
                # Modify the campaign state with a command
                #cam.command(cmd)  # TODO

            # Serialize the campaign state and send it back
            obj = {'test': 5}
            #obj = cam.serialize()  # TODO
            self.wr(json.dumps(dict(obj)))
        except Exception as e:
            self.send_response(500)
            raise e


if __name__ == "__main__":
    server = HTTPServer(("localhost", 8000), JSONRequestHandler)
    server.serve_forever()
